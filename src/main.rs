//! Модуль последовательного получения простых чисел методом проверки остатков от деления.
//! Первоначальные остатки от деления вычисляются делением,
//! а все последующие инкрементом и проверкой переполнения. 
//! Функция next_state структуры состояния State позволяет распараллеливать вычисления,
//! так как проверяет произвольный диапазон чисел (заданный в состоянии State).

use std::io;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::time::{Instant};
use std::fmt;


/// Структура текущего состояния вычислений
#[derive(Debug)]
struct State {
    sn_head: Vec<u64>, // Простые числа, необходимые для заполнения sn_tail
    sn_tail: Vec<u64>, // Простые числа в диапазоне p..n
    b_idx:   usize,    // Правый индекс для проверяемых остатков
    bound:   u64,      // Следующая граница, требующая увеличения длины rests
    p: u64,            // Текущее уже проверенное на простоту но не добавленное в вектор значение
    d: u64,            // Не линейная помеха
    n: u64,            // Последнее число для проверки на простоту
    is_simple: bool,   // Результат проверки p на простоту
}

impl State {
    /// Конструктор структуры состояния
    fn new(n:u64) -> State {
        State {
            sn_head: vec![2, 3, 5],
            sn_tail: vec![2],
            b_idx:   0,
            bound:   3*3,
            p: 7,
            d: 2,
            n,
            is_simple: true,
        }
    }
    /// Определяет простое ли число.
    fn is_simple(mut self)->State {
        self.is_simple = true;
        let r_idx = self.b_idx+1;
        for i in 0..r_idx {
            if self.p % &self.sn_head[i] == 0 {
                self.is_simple = false;
                return self;
            };
        };
        self
    }

    fn next_state(mut self)->State {
        if self.is_simple {
            self.sn_head.push(self.p);
        };
        self.d = if self.d == 2 {4} else {2};
        self.p+=self.d;
        if self.p >= self.bound {
            self.b_idx+=1;
            let b_idx = self.b_idx+1;
            self.bound = self.sn_head[b_idx]*self.sn_head[b_idx];
        };
        self = self.is_simple(); // ???
        self
    }
}

/// Вариант сериализации состояния
impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let vec = &self.sn_head;

        for (count, v) in vec.iter().enumerate() {
            if count != 0 { write!(f, "\n")?; }
            write!(f, "{}", v)?;
        }

        // Вернём значение `fmt::Result`
        write!(f, "")
    }
}


/// Запись строки в текстовый файл
fn save_to_file(fname: &str, s: &String) {
    // Запись в файл
    //
    let path = Path::new(fname);
    let display = path.display();

    // Откроем файл в режиме для записи. Возвращается `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("невозможно создать {}: {}", display, why),
        Ok(file) => file,
    };

    // Запишем строку `s` в `file`. Возвращается `io::Result<()>`
    match file.write_all(s.as_bytes()) {
        Err(why) => panic!("невозможно записать в {}: {}", display, why),
        Ok(_) => (), //println!("успешно записано в {}", display),
    }
}

fn main() {
    
    let mut n2 = String::new();

    println!("Введите правую границу вычислений.");
    io::stdin()
        .read_line(&mut n2)
        .expect("Failed to read line");
    
    let n2: u64 = n2.trim().parse().expect("Пожалуйста введите число большее 3.");
    let mut state = State::new(n2);

    print!("Работа основного алгоритма ... ");
    
    let start = Instant::now();
    while state.p <= n2 {
        state = state.next_state();
    }
    let duration = start.elapsed();
    println!("{:?}", duration);

    print!("Перевод вектора в строку ... ");
    let start = Instant::now();
    let s = format!("{}", state);
    let duration = start.elapsed();
    println!("{:?}", duration);

    print!("Сохранение в файл {} ... ", "sn.txt");
    let start = Instant::now();
    save_to_file("sn.txt", &s);
    let duration = start.elapsed();
    println!("{:?}", duration);

}

